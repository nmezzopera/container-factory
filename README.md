## Docker images and tags creation

Clone this repo, `cd` in the folder then:

```bash
this will publish to locahost:5000
./publish.sh /root/test-runner 10 2

# or publish to any GitLab instance

./publish.sh /root/test-runner 10 2 myHost

```

This will create _2_ images each with _10_ tags based on the `/root/test-runner/ project.`
